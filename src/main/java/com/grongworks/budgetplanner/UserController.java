package com.grongworks.budgetplanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.grongworks.budgetplanner.models.User;
import com.grongworks.budgetplanner.repositories.UserRepository;

@RestController
public class UserController {
    
    @Autowired
    private UserRepository userRepository;

    @PostMapping("/user")
    public ResponseEntity<User> registerNewUser(@RequestBody User newUser) {
        var newUserDb = userRepository.save(newUser);
        return new ResponseEntity<User>(newUserDb, HttpStatus.OK);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<User> getUser(@PathVariable Integer id) {
        var userFromDb = userRepository.findById(id);
        if (userFromDb.isPresent()) {
            return new ResponseEntity<User>(userFromDb.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
