package com.grongworks.budgetplanner.repositories;
import org.springframework.data.repository.CrudRepository;

import com.grongworks.budgetplanner.models.Todo;


public interface TodoRepository extends CrudRepository<Todo,Integer> {
    // CRUD Operationen werden automatisch zur Verfügung gestellt.
}
