package com.grongworks.budgetplanner.repositories;
import org.springframework.data.repository.CrudRepository;

import com.grongworks.budgetplanner.models.User;

public interface UserRepository extends CrudRepository<User, Integer> {
    // CRUD Operationen werden automatisch zur Verfügung gestellt. 
}
