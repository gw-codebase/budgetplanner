package com.grongworks.budgetplanner;

import org.springframework.web.bind.annotation.RestController;

import com.grongworks.budgetplanner.models.Todo;
import com.grongworks.budgetplanner.repositories.TodoRepository;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
public class TodoController {

    @Autowired
    private TodoRepository todoRepository;

    @PostMapping("/todo")
    public ResponseEntity<Todo> addTodo(@RequestBody Todo newTodo) {
        // save todo in db
        todoRepository.save(newTodo);
        return new ResponseEntity<Todo>(newTodo, HttpStatus.OK);
    }

    @GetMapping("/todo")
    public ResponseEntity<Todo> getTodo(@RequestParam(value = "id") int id) {
        
        Optional<Todo> todoInDb = todoRepository.findById(id);

        if (todoInDb.isPresent()) {
            System.out.println("Found todo in db: " + todoInDb.get().getDescription());
            return new ResponseEntity<Todo>(todoInDb.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @GetMapping("/todo/all")
    public ResponseEntity<Iterable<Todo>> getAllTodos() {
        Iterable<Todo> allTodos = todoRepository.findAll();
        return new ResponseEntity<Iterable<Todo>>(allTodos, HttpStatus.OK);
    }

    @PutMapping("/todo/{id}")
    public ResponseEntity<Todo> changeTodo(@PathVariable int id, @RequestBody Todo changedTodo) {
        Optional<Todo> todoInDb = todoRepository.findById(id);

        if (todoInDb.isPresent()) {
            changedTodo.setId(todoInDb.get().getId());
            Todo updatedTodo = todoRepository.save(changedTodo);
            return new ResponseEntity<Todo>(updatedTodo, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    @DeleteMapping("/todo/{id}")
    public ResponseEntity deleteTodo(@PathVariable int id) {
        Optional<Todo> todoInDb = todoRepository.findById(id);
        if (todoInDb.isPresent()) {
            System.out.println("Todo gefunden und kann gelöscht werden...");
            todoRepository.deleteById(id);
            return new ResponseEntity<>(null, HttpStatus.GONE);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    @PatchMapping("/todo/{id}/setDone")
    public ResponseEntity<Todo> setDone(@PathVariable int id, @RequestParam(value = "isDone") boolean isDone) {
        Optional<Todo> todoInDb = todoRepository.findById(id);

        if (todoInDb.isPresent()) {
            todoInDb.get().setIsDone(isDone);
            Todo updatedTodo = todoRepository.save(todoInDb.get());
            return new ResponseEntity<Todo>(updatedTodo, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
}
